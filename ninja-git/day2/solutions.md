

Assignment:

Create a script to generate a mearge conflict.

# script

![git](https://github.com/arunkundrupu1990/ninja-assignments/blob/master/ninja-git/day2/git\_01.png)

# output

![git](https://github.com/arunkundrupu1990/ninja-assignments/blob/master/ninja-git/day2/git\_02.png)

# git merge tool to fix the conflicts

![git](https://github.com/arunkundrupu1990/ninja-assignments/blob/master/ninja-git/day2/git\_03.png)


